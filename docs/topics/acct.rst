===============================
Secure Multi Account Management
===============================

Idem offers the ability to run against multiple cloud accounts and providers.
This is accomplished by assigning the cloud account and provider information
in an account file using the `acct` tool. The `acct` tool is a dependency
of idem and can be used to encrypt the file that stores account information.

As of the release of Idem 6 we have support for file based authentication.
The ability to hook into additional auth mechanisms if coming in future
releases.

Static Account Management
=========================

For this example we will use the azurerm system. Start by creating a file
that will store your credentials. This is a simple YAML file that can
store credentials for multiple providers and multiple accounts.

`creds.yml`
.. code-block:: yaml

    azurerm:
      default:
        api_key: 972u34gfnhw430598m
        user: 0942urjf0283

Now that you have a file with your creds, use the `acct` tool to encrypt it:

.. code-block:: bash

    $ acct creds.yml
    New encrypted file created at: creds.yml.fernet
    The file was encrypted with this key:
    j-ytfz45n2wRUHDZJsumtG5_Dih3b3lTA1P2apqNuFg=

Now you have an encrypted credentials file and a key to open it! Keep your key
in a safe place!

You can now run `idem` with the options `--acct-file` and `--acct-key` to enable
Idem to run with the credentials stored in the key.
