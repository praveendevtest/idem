===================================
SLS Resolver Plugins - hub.idem.sls
===================================

Idem SLS resolvers are very easy to write and can often work in just a few lines of
code. They are implemented in a nested pop subsystem under idem, this means that
you can vertical app-merge these plugins into idem if that makes the most sense.

The vast majority of the work to gather SLS files is completely generic, so adding
new sources should be very easy. For instance, the code to load from the filesystem
is just a few lines of code:

.. code-block:: python


    import os
    import shutil


    __virtualname__ = "file"


    async def cache(hub, cache_dir, source, loc):
        """
        Take a file from a location definition and cache it in the target location
        """
        full = os.path.join(source, loc)
        if full.startswith("file://"):
            full = full[7:]
        c_tgt = os.path.join(cache_dir, full.lstrip(os.sep))
        c_dir = os.path.dirname(c_tgt)
        os.makedirs(c_dir, exist_ok=True)
        if not os.path.isfile(full):
            return None
        shutil.copy(full, c_tgt)
        return c_tgt

The plugin simply needs to implement the `cache` async function. This function recives the `hub`,
`cache_dir`, `source`, and `loc`. These will be passed into the function.

The `cache_dir` is where you should store temporary files that are gathered.

The `source` is the root path to pull the file from. In the case of the file resolver
this will be `file:///path/to/sls/root`.

The `loc` or location is the desired file location relative to the `source`.
