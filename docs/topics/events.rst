Events
======

Every event follows a predictable format:

.. code-block:: json

    {
        "tags": {
            "ref": "A reference to the function on the hub that fired this event",
            "type": "An identifier to describe the nature of the message"
        },
        "message": "Message data, which can be any serializable object"
    }

Event Profiles
==============

Events in idem are published to profiles of a specific name.
Create an event profile associated with specific events to subscribe to that event with your chosen provider.
Multiple providers can be configured for the same event profile.

.. code-block:: sls

    kafka:
      event_profile_name:
        connection:
          bootstrap_servers: localhost:9092
    pika:
      event_profile_name:
        connection:
          host: localhost
          port: 5672
          login: guest
          password: guest

The same profile name can be used to create multiple profiles for the same provider.

.. code-block:: sls

    kafka:
      - event_profile_name:
          connection:
            bootstrap_servers: localhost:9092
      - event_profile_name:
          connection:
            bootstrap_servers: my_server:9092

idem-*
------

Create a profile called ``idem-*`` to receive *all* events from idem.

.. code-block:: sls

    my_provider:
      idem-*:
        provider_connection_data:


The default plugin for this matching is ``glob``.
A different acct_file wide ``match_plugin`` can be specified by adding a ``match_plugin`` keyword to your acct_file.
Read more about match plugins in `pop-evbus <https://gitlab.com/vmware/idem/evbus/-/blob/master/docs/releases/5.0.0.rst>`_.

.. code-block:: sls

    match_plugin: glob|regex
    my_provider:
      idem-*:
        provider_connection_data:

idem-status
-----------

Create an evbus provider profile called ``idem-status`` to receive events about the status of the current run.

.. code-block:: sls

    my_provider:
      idem-status:
        provider_connection_data:

Message body format for status data:

.. code-block:: json

    {
        "tags": {"ref": "idem.state.update_status", "type": "state-status"},
        "message": "FINISHED/CREATED/GATHERING/COMPILING/RUNNING/COMPILATION_ERROR/GATHER_ERROR/RUNTIME_ERROR/UNDEFINED"
    }


idem-low
--------

Create an evbus provider profile called ``idem-low`` to receive events about sls low data.

.. code-block:: sls

    my_provider:
      idem-low:
        provider_connection_data:

Message body format for low data:

.. code-block:: json

    {
        "tags": {"ref": "idem.run.init.start", "type": "state-low-data"},
        "message": [
            {
                "name": "Name of the state",
                "state": "Reference on the hub to state plugin",
                "fun": "The state function name",
                "__sls__": "The sls source",
                "__id__": "The state id, usually it will be the same as name",
                "order": 100000
            }
        ]
    }

idem-high
---------

Create an evbus provider profile called ``idem-high`` to receive events about sls rendered high data.

.. code-block:: sls

    my_provider:
      idem-high:
        provider_connection_data:

Message body format for high data:

.. code-block:: json

    {
        "message": {
            "Reference on the hub to the state plugin": {
                "__sls__": "The stem/name of the sls source",
                "The reference to the state plugin": ["The reference to the state function", {"order": 100000}]
            }
        },
        "tags": {"ref": "idem.resolve.introduce", "type": "state-high-data"}
    }


idem-state
----------

Create an evbus provider profile called ``idem-state`` to receive the pre/post state information.

.. code-block:: sls

    my_provider:
      idem-state:
        provider_connection_data:

pre
~~~

Message body format for run data:

.. code-block:: json

    {
        "message": {
            "Name of the state": {
                "The reference to the state function": {
                    "ctx": {"run_name": "The run_name specified on the cli", "test": false},
                    "kwargs": {},
                    "name": "Name of the state"
                }
            }
        },
        "tags": {"ref": "Reference on the hub to the state function that fired the event", "type": "state-pre"}
    }

post
~~~~

Message body format for run data:

.. code-block:: json

    {
            "message": {
                "changes": {"old": [], "new": []},
                "comment": "",
                "name": "Name of the state",
                "result": true
            },
            "tags": {"ref": "Reference on the hub to the state function that fired the event", "type": "state-post"}
    }

idem-chunk
----------

Create an evbus provider profile called ``idem-chunk`` to receive individual fully compiled states.

.. code-block:: sls

    my_provider:
      idem-chunk:
        provider_connection_data:

Message body format for run data:

.. code-block:: json

    {
        "name": "Name of the state",
        "state": "Reference on the hub to the state plugin",
        "fun": "Reference on the hub to the state function",
        "__id__": "The state id, usually same as name",
        "__sls__": "Tye sls source",
        "order": 100000
    }

idem-run
----------

Create an evbus provider profile called ``idem-run`` to receive run data before and after the state runs.
This event has a large payload.

.. code-block:: sls

    my_provider:
      idem-error:
        provider_connection_data:

Message body format for run data:

.. code-block:: json

    {
        "acct_profile": "default",
        "add_low": [],
        "blocks": {},
        "cache_dir": "/path/to/cache_dir",
        "errors": [],
        "files": [],
        "high": {},
        "iorder": 100000,
        "meta": {"ID_DECS": {}, "SLS": {}},
        "post_low": [],
        "render": "jinja|yaml",
        "resolved": [],
        "run_num": 1,
        "running": {},
        "runtime": "parallel",
        "sls_refs": {},
        "sls_sources": [],
        "states": {},
        "status": 1,
        "subs": ["states"],
        "test": false
    }

idem-exec
---------

Create an evbus provider profile called ``idem-exec`` to receive the returns of all idem exec modules as events.

.. code-block:: sls

    my_provider:
      idem-exec:
        provider_connection_data:

Message body format for exec data:

.. code-block:: json

    {"result": true, "ret": "Any object", "comment": "Any string"}

logger
------

Create an evbus profile called ``logger`` to receive all log messages from pop as events.

.. code-block:: sls

    my_provider:
      logger:
        provider_connection_data:

When starting idem from the command line, be sure to specify ``--log-handler=queue``.

.. code-block:: bash

    idem state state.sls --log-level=debug --log-handler=queue


Log events will be published with a routing_key/topic called "log" by default.
You can customize the routing key with ``--log-handler-options routing_key=<my_key>``.

.. code-block:: bash

    idem state state.sls --log-level=debug --log-handler=queue  --log-handler-options routing_key="<run_name>"

Firing Events
=============

from code
---------

The ``routing_key`` is the channel/topic that the event will be published to.
The ``body`` is any serializable data that comprises the main part of the event.
The ``profile`` is the ingress profile from ``acct`` that this event should be published to.

Asynchronous put:

.. code-block:: python

    async def my_func(hub):
        await hub.idem.event.put(
            routing_key="channel",
            body="Any serializable object",
            profile="idem-[plugin]",
            tags={},
        )


Synchronous put:

.. code-block:: python

    def my_func(hub):
        hub.idem.event.put_nowait(
            routing_key="channel",
            body="Any serializable object",
            profile="idem-[plugin]",
            tags={},
        )

from jinja/sls
--------------

Events can also be fired from within an idem sls file via jinja:

.. code-block:: sls

    {%- hub.idem.event.put_nowait(routing_key="channel", body={"message": "event content"}, profile="default", tags={}) %}
