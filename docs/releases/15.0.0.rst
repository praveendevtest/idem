===========
Idem 15.0.0
===========

Idem 15.0.0 introduces reconciliation loop plugin.
When invoked from the CLI, the reconciliation loop plugin re-applies the state.

The reconciliation is skipped altogether or stops when the state returns
'``result=True``' and there are no '``changes``'.

Reconciliation loop also stops if the state results/changes have not changed
during the last 3 iterations.

Reconciler Plugin
=================
Reconciler plugin provided by idem is called 'basic'.
It includes a static wait time of 3 seconds between iterations.
By default the 'none' plugin is used, which skips reconciliation.

CLI
===
The reconciler plugin is specified as an argument to the
``idem state`` CLI.

For example:

.. code-block::

    --reconciler=basic | -R=basic | -r=basic

LOOP
====
To implement a reconcile plugin provide a method like this:

.. code-block::

    async def loop(
        hub,
        name,
        sls_sources,
        render,
        runtime,
        cache_dir,
        sls,
        test,
        acct_file,
        acct_key,
        acct_profile,
    ):

The reconciler loop should return a dictionary like that:

.. code-block::

    {
        "re_runs_count": <number reconciliation loop iterations>,
        "require_re_run": <True or False>,
    }
