===========
Idem 17.0.0
===========

Idem 17.0.0 adds support for argument binding to Idem SLS structure. Argument binding references are used to set argument value of a state definition to the result
of another state execution.

.. include:: ../topics/argument_binding.rst
