from tests.integration.conftest import run_sls


def test_time(hub, code_dir):
    ret = run_sls(["time"])
    assert len(ret) == 4, "Expecting 4 results"
    result = ret.get("time_|-sleep_2s_|-sleep_2s_|-sleep")
    assert result["result"] is True
    assert result["comment"] == "Successfully slept for 2 seconds."

    result = ret.get("test_|-test delay_|-test delay_|-succeed_without_changes")
    assert result["result"] is True
    assert result["comment"] == "Success!"

    result = ret.get("time_|-invalid delay_|-invalid delay_|-sleep")
    assert result["result"] is False
    assert result["comment"] == "Duration is required."

    result = ret.get("test_|-test delay2_|-test delay2_|-succeed_without_changes")
    # TODO: should this fail?
    assert result["result"] is True
    assert result["comment"] == "Success!"
