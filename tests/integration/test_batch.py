import unittest.mock as mock
import uuid

import pytest


async def test_empty(hub):
    # Test the results of a batch run with the minimum arguments and no states
    name = uuid.uuid4()

    # Reading the states will fail
    with pytest.raises(ValueError):
        await hub.idem.state.batch(name=name, states={})

    # Verify that everything was cleaned up
    ret = hub.idem.state.status(name=name)
    assert ret == {
        "acct_profile": "default",
        "errors": [],
        "running": {},
        "status": 0,
        "status_name": "FINISHED",
        "test": False,
    }


def test_undefined(hub):
    error_log = mock.MagicMock()
    hub.log.error = error_log
    ret = hub.idem.state.status(name="undefined")
    assert ret == {
        "acct_profile": "",
        "errors": [],
        "running": {},
        "status": -4,
        "status_name": "UNDEFINED",
        "test": None,
    }
    error_log.assert_called_once_with("No idem run with Job ID: undefined")


async def test_finished(hub, event_loop):
    name = uuid.uuid4()

    # Run states defined within a dictionary in code
    task = event_loop.create_task(
        hub.idem.state.batch(
            name=name,
            states={"state name": {"test.succeed_without_changes": [{"name": "name"}]}},
            test=False,
        )
    )

    # Wait for the task to start
    await hub.pop.loop.sleep(1)

    # Get the current status of the state run
    ret = hub.idem.state.status(name=name)

    # Verify the return results of the running states
    assert ret == {
        "acct_profile": "",
        "errors": [],
        "running": {},
        "status": -4,
        "status_name": "UNDEFINED",
        "test": None,
    }

    await task


async def test_encrypted_profiles(hub, event_loop):
    # Test the results of a batch run with the minimum arguments and no states
    name = uuid.uuid4()
    acct_key = hub.crypto.fernet.generate_key()
    profiles = {"provider": {"profile": {"kw1": "v1"}}}
    encrypted_profiles = hub.crypto.fernet.encrypt(profiles, key=acct_key)

    task = event_loop.create_task(
        hub.idem.state.batch(
            name=name,
            states={"state name": {"test.succeed_without_changes": [{"name": "name"}]}},
            encrypted_profiles=encrypted_profiles,
            acct_key=acct_key,
            default_acct_profile="profile",
        )
    )

    # Wait for the task to start
    await hub.pop.loop.sleep(1)

    # Verify that everything was cleaned up
    ret = hub.idem.state.status(name=name)
    assert ret == {
        "acct_profile": "",
        "errors": [],
        "running": {},
        "status": -4,
        "status_name": "UNDEFINED",
        "test": None,
    }

    await task
