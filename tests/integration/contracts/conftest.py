import sys
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="function", name="hub")
def tpath_hub(code_dir):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.sub.add(dyne_name="idem")

        hub.pop.loop.create()

        yield hub

        hub.pop.loop.CURRENT_LOOP.close()
