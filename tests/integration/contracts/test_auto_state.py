import tempfile

from idem.idem.managed import FileDict


def test_describe(hub):
    ctx = {}

    with tempfile.NamedTemporaryFile(delete=True) as fh:
        hub.exec.tests.auto.ITEMS = FileDict(fh.name)
        # Auto-populate some values for the auto state describe
        hub.exec.tests.auto.create(ctx, "name", kw1=1, kw2=2)
        d = hub.exec.tests.auto.get(ctx, "name")
        assert d
        assert d.ret == {"kw1": 1, "kw2": 2}

        ret = hub.pop.Loop.run_until_complete(hub.idem.describe.run("tests.auto"))

    returned_states = list(ret.values())
    assert returned_states, "No states were returned"
    assert returned_states[0] == {"tests.auto.present": [{"name": "name"}, {"kw1": 1}]}


def test_sls(hub, code_dir):
    name = "test"
    render = "yaml"
    cache_dir = tempfile.mkdtemp()
    sls_dir = code_dir / "tests" / "sls"
    sls_sources = [f"file://{sls_dir}"]

    with tempfile.NamedTemporaryFile() as fh:
        hub.exec.tests.auto.ITEMS = FileDict(fh.name)
        hub.pop.Loop.run_until_complete(
            hub.idem.state.apply(
                name,
                sls_sources,
                render,
                "serial",
                ["states"],
                cache_dir,
                ["auto_state"],
                test=False,
                acct_file=None,
                acct_key=None,
                managed_state={},
            )
        )

    errors = hub.idem.RUNS[name]["errors"]
    assert not errors, errors

    ret = hub.idem.RUNS[name]["running"]
    assert ret == {
        "tests.auto_|-test-autostate-create_|-foo_|-present": {
            "__run_num": 1,
            "changes": {"new": {"param": 1, "kw1": None}},
            "new_state": {"kw1": None, "param": 1},
            "old_state": None,
            "comment": "Created 'tests.auto:foo'",
            "name": "foo",
            "result": True,
        },
        "tests.auto_|-test-autostate-present_|-foo_|-present": {
            "__run_num": 2,
            "new_state": {"kw1": None, "param": 1},
            "old_state": {"kw1": None, "param": 1},
            "changes": {},
            "comment": "'tests.auto:foo' already exists",
            "name": "foo",
            "result": True,
        },
        "tests.auto_|-test-autostate-update_|-foo_|-present": {
            "__run_num": 3,
            "changes": {
                "old": {"param": 1},
                "new": {"param": 2},
            },
            "new_state": {"kw1": None, "param": 2},
            "old_state": {"kw1": None, "param": 1},
            "comment": "Updated 'tests.auto:foo'",
            "name": "foo",
            "result": True,
        },
        "tests.auto_|-test-autostate-delete_|-foo_|-absent": {
            "__run_num": 4,
            "changes": {"old": {"param": 2, "kw1": None}},
            "new_state": None,
            "old_state": {"kw1": None, "param": 2},
            "comment": "Deleted 'tests.auto:foo'",
            "name": "foo",
            "result": True,
        },
        "tests.auto_|-test-autostate-absent_|-foo_|-absent": {
            "__run_num": 5,
            "new_state": {},
            "old_state": {},
            "changes": {},
            "comment": "'tests.auto:foo' already absent",
            "name": "foo",
            "result": True,
        },
    }


def test_exec(hub):
    with tempfile.NamedTemporaryFile(delete=True) as fh:
        hub.exec.tests.auto.ITEMS = FileDict(fh.name)
        obj = dict(foo=1, bar=2, baz=3)
        name = "taco cat"
        ctx = {}

        ret = hub.exec.tests.auto.get(ctx, name)
        assert not ret, "already exists"

        ret = hub.exec.tests.auto.create(ctx, name, **obj)
        assert ret, "Could not create"
        assert "Created" in ret.comment

        ret = hub.exec.tests.auto.get(ctx, name)
        assert ret, "does not exist"
        assert ret.ret == dict(foo=1, bar=2, baz=3, kw1=None)

        ret = hub.exec.tests.auto.update(ctx, name, foo=0)
        assert ret, "Could not update"
        assert "Updated" in ret.comment

        ret = hub.exec.tests.auto.get(ctx, name)
        assert ret, "does not exist"
        assert ret.ret == {"bar": 2, "baz": 3, "foo": 0, "kw1": None}

        ret = hub.exec.tests.auto.delete(ctx, name, **obj)
        assert ret, "Could not delete"

        ret = hub.exec.tests.auto.get(ctx, name)
        assert not ret, "still exists"
