import subprocess
import sys
import tempfile


def test_cli(runpy, tmpdir):
    cache_dir = tmpdir
    run_name = "test_restore"
    cache_file = tmpdir / f"{run_name}.json"

    try:
        with tempfile.NamedTemporaryFile(suffix=".json") as fh:
            fh.write(b'{"1":"2"}')
            fh.flush()

            # Run the restore command
            cmd = [
                sys.executable,
                runpy,
                "restore",
                fh.name,
                f"--cache-dir={cache_dir}",
                f"--run-name={run_name}",
            ]
            ret = subprocess.run(cmd, capture_output=True)

        assert not ret.returncode, ret.stderr

        # The data should have been transferred to the cache_file
        with cache_file.open("r") as fh:
            contents = fh.read()
            assert contents == '{"1": "2"}'
    finally:
        cache_file.remove()
