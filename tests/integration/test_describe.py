import ast
import json
import subprocess
import sys
import tempfile


def test_cli(runpy):
    # Run the describe command
    cmd = [sys.executable, runpy, "describe", "test", "--output=json"]
    ret = subprocess.run(cmd, capture_output=True)
    assert ret.returncode == 0, ret.stderr
    yaml = ret.stdout.decode()

    fh = tempfile.NamedTemporaryFile("w+", suffix=".sls")
    fh.write(yaml)
    fh.flush()

    # Verify that passing states were created from it
    cmd = [sys.executable, runpy, "state", fh.name, "--output=json", "--runtime=serial"]
    ret = subprocess.run(cmd, capture_output=True)
    fh.close()
    data = json.loads(ret.stdout.decode())
    assert ret.returncode == 0, ret.stderr
    assert data == {
        "test_|-Description of test.anop_|-anop_|-anop": {
            "__run_num": 1,
            "changes": {},
            "comment": "Success!",
            "name": "anop",
            "result": True,
        },
        "test_|-Description of test.configurable_test_state_|-configurable_test_state_|-configurable_test_state": {
            "__run_num": 2,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                }
            },
            "comment": "",
            "name": "configurable_test_state",
            "result": True,
        },
        "test_|-Description of test.describe_|-Description of test.describe_|-describe": {
            "Description of test.anop": {"test.anop": [{"name": "anop"}]},
            "Description of test.configurable_test_state": {
                "test.configurable_test_state": [
                    {"name": "configurable_test_state"},
                    {"changes": True},
                    {"result": True},
                    {"comment": ""},
                ]
            },
            "Description of test.describe": {"test.describe": []},
            "Description of test.fail_with_changes": {
                "test.fail_with_changes": [{"name": "fail_with_changes"}]
            },
            "Description of test.fail_without_changes": {
                "test.fail_without_changes": [{"name": "fail_without_changes"}]
            },
            "Description of test.mod_watch": {
                "test.mod_watch": [{"name": "mod_watch"}]
            },
            "Description of test.none_without_changes": {
                "test.none_without_changes": [{"name": "none_without_changes"}]
            },
            "Description of test.nop": {"test.nop": [{"name": "nop"}]},
            "Description of test.succeed_with_changes": {
                "test.succeed_with_changes": [{"name": "succeed_with_changes"}]
            },
            "Description of test.succeed_with_comment": {
                "test.succeed_with_comment": [
                    {"name": "succeed_with_comment"},
                    {"comment": None},
                ]
            },
            "Description of test.succeed_without_changes": {
                "test.succeed_without_changes": [{"name": "succeed_without_changes"}]
            },
            "Description of test.treq": {"test.treq": [{"name": "treq"}]},
            "Description of test.update_low": {
                "test.update_low": [{"name": "update_low"}]
            },
            "__run_num": 3,
        },
        "test_|-Description of test.fail_with_changes_|-fail_with_changes_|-fail_with_changes": {
            "__run_num": 4,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                }
            },
            "comment": "Failure!",
            "name": "fail_with_changes",
            "result": False,
        },
        "test_|-Description of test.fail_without_changes_|-fail_without_changes_|-fail_without_changes": {
            "__run_num": 5,
            "changes": {},
            "comment": "Failure!",
            "name": "fail_without_changes",
            "result": False,
        },
        "test_|-Description of test.mod_watch_|-mod_watch_|-mod_watch": {
            "__run_num": 6,
            "changes": {"watch": True},
            "comment": "Watch " "ran!",
            "name": "mod_watch",
            "result": True,
        },
        "test_|-Description of test.none_without_changes_|-none_without_changes_|-none_without_changes": {
            "__run_num": 7,
            "changes": {},
            "comment": "Success!",
            "name": "none_without_changes",
            "result": None,
        },
        "test_|-Description of test.nop_|-nop_|-nop": {
            "__run_num": 8,
            "changes": {},
            "comment": "Success!",
            "name": "nop",
            "result": True,
        },
        "test_|-Description of test.succeed_with_changes_|-succeed_with_changes_|-succeed_with_changes": {
            "__run_num": 9,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                },
                "tests": [[{"new": "new_test"}]],
            },
            "new_state": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                },
                "tests": [[{"new": "new_test"}]],
            },
            "old_state": {},
            "comment": "Success!",
            "name": "succeed_with_changes",
            "result": True,
        },
        "test_|-Description of test.succeed_with_comment_|-succeed_with_comment_|-succeed_with_comment": {
            "__run_num": 10,
            "changes": {},
            "comment": None,
            "name": "succeed_with_comment",
            "result": True,
        },
        "test_|-Description of test.succeed_without_changes_|-succeed_without_changes_|-succeed_without_changes": {
            "__run_num": 11,
            "changes": {},
            "comment": "Success!",
            "name": "succeed_without_changes",
            "result": True,
        },
        "test_|-Description of test.treq_|-treq_|-treq": {
            "__run_num": 14,
            "changes": {},
            "comment": "Success!",
            "name": "treq",
            "result": True,
        },
        "test_|-Description of test.update_low_|-update_low_|-update_low": {
            "__run_num": 12,
            "changes": {},
            "comment": "Success!",
            "name": "update_low",
            "result": True,
        },
        "test_|-king_arthur_|-totally_extra_alls_|-nop": {
            "__run_num": 13,
            "changes": {},
            "comment": "Success!",
            "name": "totally_extra_alls",
            "result": True,
        },
    }


def test_jmespath_output(runpy):
    cmd = [sys.executable, runpy, "describe", "test", "--output=jmespath"]

    ret = subprocess.run(cmd, capture_output=True)
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout.decode()

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.anop",
            "resource": [{"name": "anop"}],
            "ref": "test.anop",
        },
        {
            "name": "Description of test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
            "ref": "test.configurable_test_state",
        },
        {
            "name": "Description of test.describe",
            "resource": [],
            "ref": "test.describe",
        },
        {
            "name": "Description of test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
            "ref": "test.fail_with_changes",
        },
        {
            "name": "Description of test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
            "ref": "test.fail_without_changes",
        },
        {
            "name": "Description of test.mod_watch",
            "resource": [{"name": "mod_watch"}],
            "ref": "test.mod_watch",
        },
        {
            "name": "Description of test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
            "ref": "test.none_without_changes",
        },
        {
            "name": "Description of test.nop",
            "resource": [{"name": "nop"}],
            "ref": "test.nop",
        },
        {
            "name": "Description of test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
            "ref": "test.succeed_with_changes",
        },
        {
            "name": "Description of test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
            "ref": "test.succeed_with_comment",
        },
        {
            "name": "Description of test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
            "ref": "test.succeed_without_changes",
        },
        {
            "name": "Description of test.treq",
            "resource": [{"name": "treq"}],
            "ref": "test.treq",
        },
        {
            "name": "Description of test.update_low",
            "resource": [{"name": "update_low"}],
            "ref": "test.update_low",
        },
    ]


def test_jmespath_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "@",
    ]

    ret = subprocess.run(cmd, capture_output=True)
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout.decode()

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.anop",
            "resource": [{"name": "anop"}],
            "ref": "test.anop",
        },
        {
            "name": "Description of test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
            "ref": "test.configurable_test_state",
        },
        {
            "name": "Description of test.describe",
            "resource": [],
            "ref": "test.describe",
        },
        {
            "name": "Description of test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
            "ref": "test.fail_with_changes",
        },
        {
            "name": "Description of test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
            "ref": "test.fail_without_changes",
        },
        {
            "name": "Description of test.mod_watch",
            "resource": [{"name": "mod_watch"}],
            "ref": "test.mod_watch",
        },
        {
            "name": "Description of test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
            "ref": "test.none_without_changes",
        },
        {
            "name": "Description of test.nop",
            "resource": [{"name": "nop"}],
            "ref": "test.nop",
        },
        {
            "name": "Description of test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
            "ref": "test.succeed_with_changes",
        },
        {
            "name": "Description of test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
            "ref": "test.succeed_with_comment",
        },
        {
            "name": "Description of test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
            "ref": "test.succeed_without_changes",
        },
        {
            "name": "Description of test.treq",
            "resource": [{"name": "treq"}],
            "ref": "test.treq",
        },
        {
            "name": "Description of test.update_low",
            "resource": [{"name": "update_low"}],
            "ref": "test.update_low",
        },
    ]


def test_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "[?resource[?name=='succeed_with_comment']]",
    ]

    ret = subprocess.run(cmd, capture_output=True)
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout.decode()

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
            "ref": "test.succeed_with_comment",
        },
    ]
