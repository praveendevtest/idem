def comment(hub, name, ctx, comments):
    """
    Spit back out the comments passed in
    """
    return {"name": name, "changes": {}, "comment": comments, "result": True}


def mod_aggregate(hub, name, chunk):
    """
    Modify the chunk to have different comments
    """
    chunk["comments"] = "Modified by mod_aggregate and the mod system"
    return chunk
